<?php

define('AllowEndpoint', TRUE);
define('AllowHttpMethods', array('POST'));
define('RequireLogin', TRUE);

require_once __DIR__ . '/../EndpointBlocker.php';

if (!isset($_SESSION)) {
  session_start();
}

$data = json_decode(file_get_contents('php://input'), true);
$_SESSION['host_url'] = $data["host_url"];
