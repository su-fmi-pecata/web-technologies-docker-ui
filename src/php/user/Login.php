<?php

define('AllowEndpoint', TRUE);
define('AllowHttpMethods', array('POST'));
require_once __DIR__ . '/../EndpointBlocker.php';

require_once __DIR__ . '/../db/db.php';

use function DB\getDefaultDb;

if (!isset($_SESSION)) {
    session_start();
}

if (isset($_SESSION['logged_in']) && $_SESSION['logged_in']) {
    http_response_code(409);
    die("already logged in");
}

$json = file_get_contents('php://input');
$data = json_decode($json);
$username = $data->username;
$password = $data->password;

$db = getDefaultDb();

try {
    $authenticated = $db->authenticateUser($username, $password);
    if ($authenticated) {
        $_SESSION['username'] = $username;
        $_SESSION['logged_in'] = TRUE;
    } else {
        http_response_code(403);
        echo json_encode(["errors" => "bad login"]);
    }
} catch (PDOException $e) {
    http_response_code(400);
    echo json_encode(["SQLError" => $e->getMessage()]);
}
