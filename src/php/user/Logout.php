<?php

define('AllowEndpoint', TRUE);
define('AllowHttpMethods', array('POST'));
define('RequireLogin', TRUE);
require_once __DIR__ . '/../EndpointBlocker.php';

if (!isset($_SESSION)) {
    session_start();
}

unset($_SESSION['username']);
unset($_SESSION['host_url']);
$_SESSION['logged_in'] = FALSE;
