<?php

define('AllowEndpoint', TRUE);
define('AllowHttpMethods', array('POST'));
require_once __DIR__ . '/../EndpointBlocker.php';

require_once __DIR__ . '/../db/db.php';
require_once __DIR__ . '/../validations/Validations.php';

use function DB\getDefaultDb;
use function Validations\getStringLengthErrors;

$json = file_get_contents('php://input');
$data = json_decode($json);
$username = $data->username;
$password = $data->password;
$dockerURL = $data->dockerURL;
$dockerUsername = $data->dockerUsername;
$dockerPassword = $data->dockerPassword;

function getUsernameErrors($username)
{
  return getStringLengthErrors($username, 3, 256, "username");
}

function getPasswordErrors($username)
{
  return getStringLengthErrors($username, 6, 128, "password");
}

function getDockerURLErrors($username)
{
  return getStringLengthErrors($username, 3, 256, "docker URL");
}

function safeStrlen($str)
{
  return $str == NULL ? false : strlen($str);
};

$errors = [
  "username" => getUsernameErrors($username),
  "password" => getPasswordErrors($password),
  "dockerURL" => getDockerURLErrors($dockerURL),
];

$errors = array_filter($errors, 'safeStrlen');
$errorsCount = count($errors);

if ($errorsCount > 0) {
  $result = [];
  $result["errors"] = $errors;
  header('Content-Type: application/json; charset=utf-8');
  http_response_code(400);
  echo json_encode($result);
  exit;
}
$hostConfig = new HostConfig($dockerUsername, $dockerPassword, $dockerURL);

$user = new User(0, $username, "", array($hostConfig));
$user->setPasswordHash($password);

$db = getDefaultDb();

try {
  $id = $db->insertUser($user);
  $db->addHostConfig($user->getHostConfigs()[0], $id);
} catch (PDOException $e) {
  http_response_code(400);
  echo json_encode(["SQLError" => $e->getMessage()]);
}
