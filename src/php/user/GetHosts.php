<?php

define('AllowEndpoint', TRUE);
define('AllowHttpMethods', array('GET'));
define('RequireLogin', TRUE);

require_once __DIR__ . '/../EndpointBlocker.php';

require_once __DIR__ . '/../db/db.php';
require_once __DIR__ . '/../validations/Validations.php';

use function DB\getDefaultDb;
use function Validations\getStringLengthErrors;

if (!isset($_SESSION)) {
    session_start();
}

$username = $_SESSION['username'];
$db = getDefaultDb();

try {
    echo json_encode($db->getHostsForUser($username));
} catch (PDOException $e) {
    http_response_code(400);
    echo json_encode(["SQLError" => $e->getMessage()]);
}
