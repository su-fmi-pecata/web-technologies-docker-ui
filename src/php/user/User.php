<?php

require_once __DIR__ . '/../EndpointBlocker.php';

class HostConfig
{
    public string $username;
    public string $password;
    public string $url;

    public function __construct(string $username, string $password, string $url)
    {
        $this->username = $username;
        $this->password = $password;
        $this->url = $url;
    }

    public function getInsertFields()
    {
        return get_object_vars($this);
    }
}

class User
{
    private int $id;
    private string $name;
    private string $passwordHash;
    private array $hostConfigs;

    public function __construct(int $id, string $username, string $password_hash, array $hostConfigs)
    {
        $this->id = $id;
        $this->name = $username;
        $this->passwordHash = $password_hash;
        $this->hostConfigs = $hostConfigs;
    }

    public function setPasswordHash(string $password)
    {
        $this->passwordHash = password_hash($password, PASSWORD_DEFAULT);
    }

    public function getInsertFields()
    {
        $res['name'] = $this->name;
        $res['passwordHash'] = $this->passwordHash;
        return $res;
    }

    public function getId(): string
    {
        return $this->id;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function getPasswordHash(): string
    {
        return $this->passwordHash;
    }

    public function getHostConfigs(): array
    {
        return $this->hostConfigs;
    }
}
