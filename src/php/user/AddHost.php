<?php

define('AllowEndpoint', TRUE);
define('AllowHttpMethods', array('POST'));
define('RequireLogin', TRUE);

require_once __DIR__ . '/../EndpointBlocker.php';
require_once __DIR__ . '/../db/db.php';

use function DB\getDefaultDb;

if (!isset($_SESSION)) {
    session_start();
}

$json = file_get_contents('php://input');
$data = json_decode($json);
$dockerURL = $data->dockerURL;
$dockerUsername = $data->dockerUsername;
$dockerPassword = $data->dockerPassword;

$db = getDefaultDb();

try {
    $hostConf = new HostConfig($dockerUsername, $dockerPassword, $dockerURL);
    $user = $db -> getUserByName($_SESSION['username']);
    $db->addHostConfig($hostConf, $user->getId());
} catch (Exception $e) {
    http_response_code(400);
    echo json_encode(["SQLError" => $e->getMessage()]);
}
