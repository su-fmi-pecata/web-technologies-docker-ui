<?php

namespace DockerClient;

require_once __DIR__ . '/../EndpointBlocker.php';

class Client
{
    public static function executeCommand(Config $config, string $url, string $httpMethod, array $data = null, bool $decodeJson = false)
    {
        $curlClient = curl_init();

        switch ($httpMethod) {

            case "GET":
                curl_setopt($curlClient, CURLOPT_HTTPGET, true);
                break;

            case "POST":
                curl_setopt($curlClient, CURLOPT_POSTFIELDS, true);
                if ($data != null) {
                    $payload = json_encode($data);
                    curl_setopt($curlClient, CURLOPT_POSTFIELDS, $payload);
                    curl_setopt(
                        $curlClient,
                        CURLOPT_HTTPHEADER,
                        array(
                            'Content-Type: application/json',
                            'Content-Length: ' . strlen($payload)
                        )
                    );
                }
                break;
            case "DELETE":
                curl_setopt($curlClient, CURLOPT_CUSTOMREQUEST, 'DELETE');
                break;
            default:
                die("Unknown http httpMethod exception");
        }

        $isHostUnixSocket = \preg_match('/docker.sock$/', $config->getHostUrl());
        if ($isHostUnixSocket) {
            curl_setopt($curlClient, CURLOPT_UNIX_SOCKET_PATH, $config->getHostUrl());
            curl_setopt($curlClient, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($curlClient, CURLOPT_URL, sprintf('http://unixsocket%s', $url));
        } else {
            curl_setopt($curlClient, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($curlClient, CURLOPT_URL, sprintf('%s%s', $config->getHostUrl(), $url));
        }

        $result = curl_exec($curlClient);

        if ($decodeJson) {
            return json_decode($result, true);
        }

        return $result;
    }
}
