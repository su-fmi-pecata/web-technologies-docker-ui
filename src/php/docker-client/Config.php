<?php

namespace DockerClient;

require_once __DIR__ . '/../EndpointBlocker.php';

class Config {
    private $hostUrl;
    private $username;
    private $password;

    public function __construct(string $hostUrl, string $username, string $password) {
        $this->hostUrl = $hostUrl;
        $this->username = $username;
        $this->password = $password;
    }

    public function getHostUrl() {
        return $this->hostUrl;
    }
}
?>