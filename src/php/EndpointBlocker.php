<?php
if (!defined('AllowEndpoint')) {
    http_response_code(404);
    die('Direct access not permitted ' . $_SERVER['REQUEST_URI']);
}

if (!defined('AllowHttpMethods') || !in_array($_SERVER["REQUEST_METHOD"], constant('AllowHttpMethods'))) {
    http_response_code(405);
    die('method ' . $_SERVER['REQUEST_METHOD'] . ' not allowed');
}

if (defined('RequireLogin') && constant('RequireLogin')) {
    if (!isset($_SESSION)) {
        session_start();
    }

    if (!isset($_SESSION['logged_in']) || !$_SESSION['logged_in']) {
        http_response_code(403);
        die('Not logged in');
    }
}
