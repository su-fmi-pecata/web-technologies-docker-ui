<?php

namespace DB;

require_once __DIR__ . '/../EndpointBlocker.php';
require_once __DIR__ . '/Config.php';
require_once __DIR__ . '/../user/User.php';

class DB
{
  const USERS_TABLE = "users";
  const HOSTS_TABLE = "hosts";

  const ID_COLUMN = "id";
  const NAME_COLUMN = "username";
  const PASSWORD_COLUMN = "password_hash";

  const HOST_URL_COLUMN = "host_url";
  const DOCKER_USERNAME_COLUMN = "docker_username";
  const DOCKER_PASSWORD_COLUMN = "docker_password";
  const USER_FK_COLUMN = "user_id";

  private \PDO $connection;

  public function __construct(Config $config)
  {

    $this->connection = new \PDO(
      $config->getConnectionString(),
      $config->getUser(),
      $config->getPassword(),
      [
        \PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8",
        \PDO::ATTR_DEFAULT_FETCH_MODE => \PDO::FETCH_ASSOC,
      ]
    );
  }

  public function getUserByName(string $name)
  {
    $sql = "SELECT * " .
      " FROM " . self::USERS_TABLE .
      " WHERE " .  self::NAME_COLUMN . " = :name;";
    $stmt = $this->connection->prepare($sql);
    $stmt->execute(array('name' => $name));
    $res = $stmt->fetch();

    $hosts = $this->getHostsForUser($name);
    return new \User($res[self::ID_COLUMN], $res[self::NAME_COLUMN], $res[self::PASSWORD_COLUMN], $hosts);
  }

  // insertUser add to user collection user's name and password hash and return id of the new row
  public function insertUser(\User $user): string
  {
    $sql = "INSERT INTO " . self::USERS_TABLE . " (" .
      self::NAME_COLUMN . " , " . self::PASSWORD_COLUMN .
      ") VALUES (:name, :passwordHash);";
    $stmt = $this->connection->prepare($sql);
    $insertFields = $user->getInsertFields();
    $stmt->execute($insertFields);
    return $this->connection->lastInsertID();
  }

  public function deleteUserByID(int $id)
  {
    $sql = "DELETE FROM users WHERE id = :id;";
    $stmt = $this->connection->prepare($sql);
    $stmt->execute(array('id' => $id));
  }

  public function authenticateUser(string $username, $password)
  {
    $user = $this->getUserByName($username);
    return password_verify($password, $user->getPasswordHash());
  }

  public function addHostConfig(\HostConfig $hostConfig, string $user_id): string
  {
    $sql = "INSERT INTO " . self::HOSTS_TABLE . " (" .
      self::DOCKER_USERNAME_COLUMN . ", " .
      self::DOCKER_PASSWORD_COLUMN . ", " .
      self::HOST_URL_COLUMN . ", " .
      self::USER_FK_COLUMN . ") VALUES (:username, :password, :url, :user_id);";

    $stmt = $this->connection->prepare($sql);
    $insertFields = $hostConfig->getInsertFields();
    $insertFields["user_id"] = $user_id;
    $stmt->execute($insertFields);
    return $this->connection->lastInsertId();
  }

  public function getHostsForUser(string $username)
  {
    $sql = "SELECT " . self::DOCKER_USERNAME_COLUMN . ", " . self::HOST_URL_COLUMN .
      " FROM " . self::HOSTS_TABLE . " h " .
      " JOIN " . self::USERS_TABLE . " u " .
      " ON  " . self::USER_FK_COLUMN . " = u." . self::ID_COLUMN .
      " WHERE  " . self::NAME_COLUMN . " = :username";
    $stmt = $this->connection->prepare($sql);
    $stmt->execute(array('username' => $username));
    return $stmt->fetchAll();
  }
}

function getDefaultDb()
{
  $global_config = json_decode(file_get_contents(__DIR__ . '/../../config.json'));
  $db_config = new MYSQLConfig($global_config->host, $global_config->user, $global_config->password, $global_config->dbName);
  return new DB($db_config);
}
