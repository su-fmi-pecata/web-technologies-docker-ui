<?php

namespace DB;

require_once __DIR__ . '/../EndpointBlocker.php';

abstract class Config
{
  protected string $host;
  protected string $user;
  protected string $pass;
  protected string $dbName;

  abstract public function getConnectionString();

  public function getUser()
  {
    return $this->user;
  }

  public function getPassword()
  {
    return $this->pass;
  }
}

class MYSQLConfig extends Config
{

  public function __construct(string $host, string $user, string $pass, string $dbName)
  {
    $this->host = $host;
    $this->user = $user;
    $this->pass = $pass;
    $this->dbName = $dbName;
  }

  public function getConnectionString()
  {
    return "mysql:host=$this->host;dbname=$this->dbName";
  }
}
