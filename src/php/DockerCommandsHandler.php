<?php

define('AllowEndpoint', TRUE);
define('AllowHttpMethods', array('GET', 'POST'));
define('RequireLogin', TRUE);
require_once __DIR__ . '/EndpointBlocker.php';

require_once('docker-client/index.php');

use DockerClient\Client;
use DockerClient\Config;

if (!$_GET) {
    die("no command passed");
}

if (!isset($_SESSION)) {
    session_start();
}

$command = $_GET['command'];
header('Content-Type: application/json; charset=utf-8');

$data = json_decode(file_get_contents('php://input'), true);
$config = new Config($_SESSION['host_url'], '', '');
echo ExecuteCommand($config, $command, $data);

function ExecuteCommand(Config $config, string $command, array $data = null)
{
    $id = '';
    if (isset($data) && isset($data["Id"])) {
        $id = $data["Id"];
    }
    switch ($command) {
        case "listImages":
            return Client::executeCommand($config, '/images/json', 'GET');
        case "listContainers":
            return Client::executeCommand($config, '/containers/json?all=true', 'GET');
        case "listVolumes":
            return Client::executeCommand($config, '/volumes', 'GET');
        case "createContainer":
            return Client::executeCommand($config, '/containers/create', 'POST', $data);
        case "startContainer":
            return Client::executeCommand($config, "/containers/$id/start", 'POST');
        case "stopContainer":
            return Client::executeCommand($config, "/containers/$id/stop", 'POST');
        case "pauseContainer":
            return Client::executeCommand($config, "/containers/$id/pause", 'POST');
        case "unpauseContainer":
            return Client::executeCommand($config, "/containers/$id/unpause", 'POST');
        case "deleteContainer":
            return Client::executeCommand($config, "/containers/$id", 'DELETE');
        case "deleteImage":
            return Client::executeCommand($config, "/images/$id", 'DELETE');
        case "createImage":
            return Client::executeCommand($config, "/images/create", 'POST', $data);
        case "buildImage":
            return Client::executeCommand($config, "/build", 'POST', $data);
        case "inspectImage":
            return Client::executeCommand($config, "/images/$id/json", 'GET');
        case "deleteVolume":
            return Client::executeCommand($config, "/volumes/$id", 'DELETE');
        case "inspectVolume":
            return Client::executeCommand($config, "/volumes/$id", 'GET');
        default:
            die("unknown command");
    }
}
