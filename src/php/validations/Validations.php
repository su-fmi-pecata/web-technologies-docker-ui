<?php

namespace Validations;

require_once __DIR__ . '/../EndpointBlocker.php';

function getStringLengthErrors($str, $min, $max, $key)
{
  $len = strlen($str);

  if ($len == 0) {
    return "$key is mandatory";
  }

  if ($min != NULL && $len < $min) {
    return "Min length for $key is $min";
  }

  if ($max != NULL && $len > $max) {
    return "Max length for $key is $max";
  }

  return NULL;
}
