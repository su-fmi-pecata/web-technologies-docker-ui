<?php

require_once __DIR__ . '/../PageBlocker.php';
require_once __DIR__ . '/../Page.php';

class RegistrationPage extends Page
{
    public function getTitle()
    {
        return "Register";
    }

    public function getContent()
    {
        return <<<HTML
            <h1>Register</h1>
            <form id="register-form" > 
                <form-group>
                    <label>Username</label>
                    <input type="text" name="username" minlength=3 maxlength=256 required>
                </form-group>
                <form-group>
                    <label>Password</label>
                    <input type="password" name="password" minlength=6 required>
                </form-group>
                <form-group>
                    <label>URL To Docker Daemon</label>
                    <input type="text" name="dockerURL" minlength=3 maxlength=256 required>
                </form-group>
                  <form-group>
                    <label>Username To Docker Daemon</label>
                    <input type="text" name="dockerUsername" maxlength=256>
                </form-group>
                  <form-group>
                    <label>Password To Docker Daemon</label>
                    <input type="password" name="dockerPassword" maxlength=256>
                </form-group>
                <form-group>
                    <button class="btn-accent btn card card-1" type="submit">
                        <span>Register</span>
                    </button>
                </form-group>
            </form>
        HTML;
    }
    public function getHeadElements()
    {
        return <<<HTML
            <script type="module" src='../js/register.js'></script>
        HTML;
    }
};
