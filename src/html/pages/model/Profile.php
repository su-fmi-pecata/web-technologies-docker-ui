<?php

require_once __DIR__ . '/../PageBlocker.php';
require_once __DIR__ . '/../Page.php';

class ProfilePage extends Page
{

    public function defineConstants()
    {
        define('RequireLogin', TRUE);
        define('SkipHostCheck', TRUE);
    }

    public function getTitle()
    {
        return "Profile";
    }

    public function getContent()
    {
        return <<<HTML
            <div id="addHost"></div>
            <dynamic-table accessor='hostsDynamicTable'></dynamic-table>
        HTML;
    }
    public function getHeadElements()
    {
        return <<<HTML
            <script type="module" src='../js/profile.js'></script>
        HTML;
    }
};
