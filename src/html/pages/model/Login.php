<?php

require_once __DIR__ . '/../PageBlocker.php';
require_once __DIR__ . '/../Page.php';

class LoginPage extends Page
{
    public function getTitle()
    {
        return "LoginPage";
    }

    public function getContent()
    {
        return <<<HTML
            <h1> Login </h1>
            <form id="login-form">
                <form-group>
                    <label>Username</label>
                    <input type="text" name="username" required>
                </form-group>
                <form-group>
                    <label>Password</label>
                    <input type="password" name="password" required>
                </form-group>
                <form-group>
                    <button class="btn-accent btn card card-1" type="submit">
                        <span>Login</span>
                    </button>
                </form-group>
            </form>
        HTML;
    }

    public function getHeadElements()
    {
        return <<<HTML
            <script type="module" src='../js/login.js'></script>
        HTML;
    }
}
