<?php

require_once __DIR__ . '/../PageBlocker.php';
require_once __DIR__ . '/../Page.php';

class ImagesPage extends Page
{

    public function defineConstants() {
        define('RequireLogin', TRUE);
    }

    public function getTitle()
    {
        return "Images";
    }

    public function getContent()
    {
        return <<<HTML
            <dynamic-table accessor='imagesDynamicTable'></dynamic-table>
        HTML;
    }
    public function getHeadElements()
    {
        return <<<HTML
            <script type="module" src='../js/images.js'></script>
        HTML;
    }
};
