<?php

require_once __DIR__ . '/../PageBlocker.php';
require_once __DIR__ . '/../Page.php';

class ContainersPage extends Page
{

    public function defineConstants() {
        define('RequireLogin', TRUE);
    }

    public function getTitle()
    {
        return "Containers";
    }

    public function getContent()
    {
        return <<<HTML
            <dynamic-table accessor='cotainersDynamicTable'></dynamic-table>
        HTML;
    }
    public function getHeadElements()
    {
        return <<<HTML
            <script type="module" src='../js/containers.js'></script>
        HTML;
    }
};
