<?php

require_once __DIR__ . '/../PageBlocker.php';
require_once __DIR__ . '/../Page.php';

class HomePage extends Page
{
    public function getTitle()
    {
        return "HomePage";
    }

    public function getContent()
    {
        return <<<HTML
            <img src="../images/docker_logo.png" alt="Docker logo" style="max-width: 100%;" >
        HTML;
    }
};
