<?php

require_once __DIR__ . '/../PageBlocker.php';
require_once __DIR__ . '/../Page.php';

class VolumesPage extends Page
{

    public function defineConstants() {
        define('RequireLogin', TRUE);
    }

    public function getTitle()
    {
        return "Volumes";
    }

    public function getContent()
    {
        return <<<HTML
            <dynamic-table accessor='volumesDynamicTable'></dynamic-table>
        HTML;
    }
    public function getHeadElements()
    {
        return <<<HTML
            <script type="module" src='../js/volumes.js'></script>
        HTML;
    }
};
