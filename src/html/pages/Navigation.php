<?php

require_once __DIR__ . '/PageBlocker.php';

?>

<nav id="main-nav">
    <section>
        <ul>
            <router-link page='containers'>Containers</router-link>
            <router-link page='images'>Images</router-link>
            <router-link page='volumes'>Volumes</router-link>
        </ul>
    </section>
    <section>
        <ul>
            <?php
            if (!isset($_SESSION)) {
                session_start();
            }

            if (isset($_SESSION['username'])) {
                $username = $_SESSION['username'];

                echo '<router-link page="profile"> ' . $username . ' </router-link>';
                echo '<router-link class="skip-adding-on-click logout-btn">Logout</router-link>';
            } else {
                echo '<router-link page="login">Login</router-link>';
                echo '<router-link page="register">Register</router-link>';
            }
            ?>
        </ul>
    </section>
</nav>