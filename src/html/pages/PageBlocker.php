<?php

if (!defined('AlreadyExecutedOncePageBlockerImport')) {
    function redirect($page, $message)
    {
        $homePage = '/html/';
        $url = substr($_SERVER["SCRIPT_NAME"], 0, strrpos($_SERVER["SCRIPT_NAME"], $homePage) + strlen($homePage));
        if ($page != NULL) {
            $url = "$url?page=$page";
        }
        header("Location: $url", true, 301);
        die($message);
    }

    define('AlreadyExecutedOncePageBlockerImport', TRUE);
}

if (!defined('AllowPage')) {
    redirect(NULL, 'Direct access not permitted');
}

if (defined('RequireLogin') && constant('RequireLogin')) {
    if (!isset($_SESSION)) {
        session_start();
    }

    if (!isset($_SESSION['logged_in']) || !$_SESSION['logged_in']) {
        redirect("403", 'Not logged in');
    }

    if (!defined('SkipHostCheck') && !isset($_SESSION['host_url'])) {
        redirect("profile", "Host not selected");
    }
}
