<?php

require_once __DIR__ . '/PageBlocker.php';

abstract class Page
{
    abstract public function getTitle();
    abstract public function getContent();

    public function getHeadElements() {
        return '';
    }

    public function getStatusCode() {
        return 200;
    }

    public function defineConstants() {
        
    }
}
