<?php

require_once __DIR__ . '/../PageBlocker.php';
require_once __DIR__ . '/../Page.php';

class Page404 extends Page
{
    public function getTitle()
    {
        return "Not Found";
    }

    public function getContent()
    {
        return <<<HTML
            <h1>Page was not found</h1>
        HTML;
    }

    public function getStatusCode() {
        return 404;
    }
};
