<?php

require_once __DIR__ . '/../PageBlocker.php';
require_once __DIR__ . '/../Page.php';

class Page403 extends Page
{
    public function getTitle()
    {
        return "Forbidden";
    }

    public function getContent()
    {
        if (!isset($_SESSION)) {
            session_start();
        }

        $additionalInfo = '';
        if (!isset($_SESSION['username'])) {
            $additionalInfo = "You are currently not logged in.";
        }

        return <<<HTML
            <h1>Page is forbidden</h1>
            ${additionalInfo}
        HTML;
    }

    public function getStatusCode() {
        return 403;
    }
};
