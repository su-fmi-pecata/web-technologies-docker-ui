<?php

require_once __DIR__ . '/PageBlocker.php';

foreach (glob(__DIR__ . "/model/*.php") as $filename) {
    require_once $filename;
}

foreach (glob(__DIR__ . "/error/*.php") as $filename) {
    require_once $filename;
}

class PageFactory
{
    public static function getPage(string $pageName)
    {
        switch ($pageName) {
            case 'index':
            case 'home':
            case '':
                return new HomePage();

            case 'containers':
                return new ContainersPage();

            case 'images':
                return new ImagesPage();

            case 'register':
                return new RegistrationPage();

            case 'login':
                return new LoginPage();

            case 'volumes':
                return new VolumesPage();

            case 'profile':
                return new ProfilePage();

            case '403':
                return new Page403();

            default:
                return new Page404();
        }
    }
}
