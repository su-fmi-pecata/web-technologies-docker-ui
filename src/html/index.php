<!DOCTYPE html>
<html>

<?php
define("AllowPage", TRUE);
require_once __DIR__ . '/pages/PageFactory.php';
$page = PageFactory::getPage(isset($_GET['page']) ? $_GET['page'] : '');
$page->defineConstants();
require __DIR__ . '/pages/PageBlocker.php'; // force execute
http_response_code($page->getStatusCode());
?>

<head>
    <title><?php echo $page->getTitle(); ?></title>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <link rel="stylesheet" href="http://fonts.cdnfonts.com/css/roboto">
    <script src="https://kit.fontawesome.com/1a1b82052d.js" crossorigin="anonymous"></script>
    
    <link rel='stylesheet' href='../css/main.css' />
    <link rel='stylesheet' href='../css/navigation.css' />
    <link rel='stylesheet' href='../css/button.css' />
    <link rel='stylesheet' href='../css/form.css' />
    <link rel='stylesheet' href='../css/modal.css' />
    <link rel='stylesheet' href='../css/table.css' />
    
    <script type="module" src='../js/navigation-init.js'></script>
    <script type="module" src='../js/modal-init.js'></script>
    <script type="module" src='../js/logout.js'></script>

    <?php echo $page->getHeadElements(); ?>

    <!-- table-init MUST BE LOADED LAST -->
    <script type="module" src='../js/table-init.js'></script>
</head>

<body>
    <?php require_once __DIR__ . '/pages/Navigation.php' ?>

    <main class="card card-2">
        <?php echo $page->getContent(); ?>
    </main>

    <footer>
    </footer>
</body>

</html>