CREATE TABLE IF NOT EXISTS users (
  id INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
  username  VARCHAR(256) UNIQUE, 
  password_hash VARCHAR(128) NOT NULL
);

CREATE TABLE IF NOT EXISTS hosts (
    id INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
    docker_username VARCHAR(256), 
    docker_password VARCHAR(256),
    host_url VARCHAR(256) NOT NULL,
    user_id INT NOT NULL,
    UNIQUE KEY (host_url, user_id),
    FOREIGN KEY (user_id) REFERENCES users(id) ON DELETE CASCADE
);