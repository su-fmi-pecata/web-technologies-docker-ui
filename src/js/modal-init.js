import {
    modalRootTag,
    modalTriggerTag,
    openModal
} from './modules/modal.mjs';

function getParent(element, tag, skipAttribute) {
    if (!element) return element;

    if (element.tagName === tag) {

        if (skipAttribute && element.hasAttribute(skipAttribute)) {
            return null;
        }

        return element;
    }
    return getParent(element.parentElement, tag, skipAttribute);
}

document.addEventListener("click", (e) => {
    const trigger = getParent(e.target, modalTriggerTag.toUpperCase());
    if (!trigger) return;
    
    const modal = getParent(e.target, modalRootTag.toUpperCase(), 'skip-default-behaviour');
    if (!modal) return;
    
    openModal({ modal });
});