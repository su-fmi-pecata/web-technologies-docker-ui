export function getButton({ title, color, innerHtml, onclick, classes = [] }) {
    const button = document.createElement("button");
    button.classList.add("btn", `btn-${color}`);
    if (classes && classes.length > 0) button.classList.add(classes);
    if (title) button.title = title;
    if (innerHtml) button.innerHTML = innerHtml;
    button.onclick = onclick;
    return button;
}

export function getIconButton({ title, color, icon, onclick }) {
    const innerHtml = `<i class="fa-solid ${icon}"></i>`;
    const button = getButton({ title, color, innerHtml, onclick, classes: ["btn-icon"] });
    button.style.width = "40px";
    return button;
}

export function getDeleteIconButton({ onclick }) {
    return getIconButton({
        color: "error",
        title: "Delete",
        icon: "fa-trash",
        onclick,
    });
}

export function getPauseIconButton({ onclick }) {
    return getIconButton({
        color: "info",
        title: "Pause",
        icon: "fa-pause",
        onclick,
    });
}

export function getStartIconButton({ onclick }) {
    return getIconButton({
        color: "success",
        title: "Start",
        icon: "fa-play",
        onclick,
    });
}

export function getStopIconButton({ onclick }) {
    return getIconButton({
        color: "warn",
        title: "Stop",
        icon: "fa-stop",
        onclick,
    });
}

export function getUnpauseIconButton({ onclick }) {
    return getIconButton({
        color: "trigger",
        title: "Unpause",
        icon: "fa-play",
        onclick,
    });
}

export function getInfoIconButton({ onclick, title }) {
    return getIconButton({
        color: "info",
        title: title || "Info",
        icon: "fa-info",
        onclick,
    });
}

export function getAddIconButton({ onclick }) {
    return getIconButton({
        color: "success",
        title: "Add",
        icon: "fa-plus",
        onclick,
    });
}
