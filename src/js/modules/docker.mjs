import { get, post } from "./http.mjs";

const commandsHandlerPath = "/php/DockerCommandsHandler.php";
const getRequestCommand = (command) => ({
  url: commandsHandlerPath,
  params: { command },
});
const postRequestCommand = (command, data) => ({
  url: commandsHandlerPath,
  params: { command },
  data,
});

export async function getContainers() {
  return await get(getRequestCommand("listContainers"));
}
export async function getImages() {
  return await get(getRequestCommand("listImages"));
}
export async function getVolumes() {
  let result = await get(getRequestCommand("listVolumes"));
  return result.Volumes;
}

export async function getHosts() {
  return await get({ url: "/php/user/GetHosts.php" });
}

export async function createContainer(data) {
  return await post(postRequestCommand("createContainer", data));
}

export async function startContainer(data) {
  return await post(postRequestCommand("startContainer", data));
}

export async function stopContainer(data) {
  return await post(postRequestCommand("stopContainer", data));
}

export async function pauseContainer(data) {
  return await post(postRequestCommand("pauseContainer", data));
}

export async function unpauseContainer(data) {
  return await post(postRequestCommand("unpauseContainer", data));
}

export async function deleteContainer(data) {
  return await post(postRequestCommand("deleteContainer", data));
}

export async function deleteImage(data) {
  return await post(postRequestCommand("deleteImage", data));
}

export async function createImage(data) {
  return await post(postRequestCommand("createImage", data));
}

export async function buildImage(data) {
  return await post(postRequestCommand("buildImage", data));
}

export async function inspectImage(data) {
  return await get(getRequestCommand("inspectImage", data));
}

export async function deleteVolume(data) {
  return await post(postRequestCommand("deleteVolume", data));
}

export async function inspectVolume(data) {
  return await post(postRequestCommand("inspectVolume", data))
}