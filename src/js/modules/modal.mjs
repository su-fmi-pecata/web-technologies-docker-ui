export const modalRootTag = "modal";
export const modalTriggerTag = "modal-trigger";
const modalContainerTag = "modal-container";
export const modalMainTag = "modal-main";
const modalCloseTag = "modal-close";

export function openModal({ modal }) {
    const container = modal.querySelector(modalContainerTag);
    const main = modal.querySelector(modalMainTag);

    const btnClose = document.createElement(modalCloseTag);
    btnClose.innerHTML = '<i class="fa-solid fa-xmark"></i>';

    const close = (e) => {
        container.style.display = "none";
        container.onclick = null;
        main.onclick = null;
        btnClose.remove();
    }

    btnClose.onclick = close;
    container.onclick = close;
    main.onclick = e => e.stopPropagation();

    main.appendChild(btnClose);
    container.style.display = "block";
}

export function createModal({ trigger, bodyHtml, bodyChild }) {
    const modalHtml = `
        <${modalTriggerTag}></${modalTriggerTag}>
        <${modalContainerTag}>
            <${modalMainTag}>
            </${modalMainTag}>
        </${modalContainerTag}>
    `;
    const modal = document.createElement(modalRootTag);
    modal.innerHTML = modalHtml;

    const triggerElement = modal.querySelector(modalTriggerTag);
    triggerElement.appendChild(trigger);

    const main = modal.querySelector(modalMainTag);
    if (bodyChild) {
        main.appendChild(bodyChild);
    } else {
        main.innerHTML = bodyHtml;
    }

    return modal;
}