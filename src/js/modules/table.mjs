export class Table {
    constructor({name, columns, dataFetcher, reloadSeconds = 0, dataCompareFunction = () => true}) {
        this.name = name;
        this.columns = columns;
        this.dataFetcher = dataFetcher;
        this.reloadSeconds = reloadSeconds;
        this.dataCompareFunction = dataCompareFunction;
    }
}