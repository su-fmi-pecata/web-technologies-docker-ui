import config from './config.mjs';

export async function request({ method, url, data, params, consumeJSON = true }) {
    const requestUrl = new URL(config.baseUrl + url);
    if (params) {
        requestUrl.search = new URLSearchParams(params).toString();
    }

    const requestParameters = { method, headers: {} };

    if (data) {
        requestParameters.headers['Content-Type'] = 'application/json;charset=utf-8';
        requestParameters.body = JSON.stringify(data);
    }
    const resp = await fetch(requestUrl, requestParameters);
    return consumeJSON ? resp.json() : resp;
}

export async function get({ url, params }) {
    return request({ method: 'GET', url, params });
}

export async function post({ url, params, data, consumeJSON = true }) {
    return request({ method: 'POST', url, params, data, consumeJSON });
}

export function changePage(page) {
    const url = new URL(window.location.href);
    const searchParams = new URLSearchParams(url.search);
    searchParams.set("page", page);
    url.search = searchParams.toString();
    window.location.href = url.toString();
}