import { Table } from './modules/table.mjs';
import {
    createContainer,
    deleteImage,
    getImages,
} from './modules/docker.mjs';
import { getButton, getDeleteIconButton, getStartIconButton } from './modules/button.mjs';
import { getRelativeTimeSince } from './modules/time.mjs';
import { createModal } from './modules/modal.mjs';

function getDeleteButton({ Id }) {
    return getDeleteIconButton({
        onclick: (e) =>
            deleteImage({ Id }).then((resp) => {
                if (resp["message"]) {
                    alert(resp["message"]);
                } else {
                    alert("Deleted image");
                }
            }),
    });
}

function getStartButton({ Id }) {
    const createFn = (e) =>
        createContainer({ Image: Id }).then((resp) => {
            if (resp["message"]) {
                alert(resp["message"]);
            } else {
                alert(`Created container with ID:\n ${resp.Id}`);
            }
        });

    const container = document.createElement("span");
    container.innerHTML = `
    <h1>Create new container</h1>
    `;

    const button = getButton({
        title: "Create image",
        color: 'success',
        innerHtml: `<i class="fa-solid fa-plus"></i> Create new container`,
        onclick: createFn,
    });

    container.appendChild(button);

    return createModal({
        trigger: getStartIconButton({}),
        bodyChild: container
    });
}

function getActionsCell(args) {
    const container = document.createElement("span");
    container.appendChild(getStartButton(args));
    container.appendChild(getDeleteButton(args));
    return container;
}

window['imagesDynamicTable'] = new Table({
    name: 'Available images',
    columns: [
        {
            name: 'Tag',
            accessor: 'RepoTags[0]',
            title: ({RepoTags}) => RepoTags[0],
        },
        {
            name: 'Created At',
            accessor: ({ Created }) => getRelativeTimeSince(new Date(Created * 1000)),
            title: ({ Created }) => new Date(Created * 1000).toISOString(),
            width: 170,
        },
        {
            name: "Actions",
            accessor: getActionsCell,
            width: 108,
        },
    ],
    dataFetcher: getImages,
    reloadSeconds: 5,
    dataCompareFunction: (d1, d2) => {
        return (
            d1.length === d2.length &&
            Object.keys(d1).every((key) => {
                const a = d1[key];
                const b = d2[key];
                return a.Id == b.Id;
            })
        )
    }
});
