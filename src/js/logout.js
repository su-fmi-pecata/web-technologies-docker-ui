import { post, changePage } from './modules/http.mjs';

const logout = async (e) => {
    e.preventDefault();
    await post({ url: "/php/user/Logout.php", consumeJSON: false });
    changePage('home');
}
const logoutBtn = document.getElementsByClassName('logout-btn');

for (const b of logoutBtn) {
    b.onclick = logout;
}