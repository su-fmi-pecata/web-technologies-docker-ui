import { changePage } from './modules/http.mjs';

const routerLinks = document.getElementsByTagName('router-link');

for (const l of routerLinks) {
    if (!l.classList.contains("skip-adding-on-click")) {
        l.onclick = () => changePage(l.getAttribute('page'));
    }
    l.classList.add("btn");
}