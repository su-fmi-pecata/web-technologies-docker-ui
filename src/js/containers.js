import { Table } from "./modules/table.mjs";
import {
  getContainers,
  startContainer,
  stopContainer,
  deleteContainer,
  pauseContainer,
  unpauseContainer,
} from "./modules/docker.mjs";
import {
  getDeleteIconButton,
  getPauseIconButton,
  getStartIconButton,
  getStopIconButton,
  getUnpauseIconButton,
} from "./modules/button.mjs";

const containerStates = {
  CREATED: {
    key: "created",
    icon: "fa-plus",
    buttons: {
      start: true,
      pause: false,
      stop: false,
      unpause: false,
    },
  },
  RUNNING: {
    key: "running",
    icon: "fa-person-running",
    buttons: {
      start: false,
      pause: true,
      stop: true,
      unpause: false,
    },
  },
  RESTARTING: {
    key: "restarting",
    icon: "fa-rotate",
    buttons: {
      start: false,
      pause: false,
      stop: true,
      unpause: false,
    },
  },
  EXITED: {
    key: "exited",
    icon: "fa-xmark",
    buttons: {
      start: true,
      pause: false,
      stop: false,
      unpause: false,
    },
  },
  PAUSED: {
    key: "paused",
    icon: "fa-pause",
    buttons: {
      start: false,
      pause: false,
      stop: false,
      unpause: true,
    },
  },
  DEAD: {
    key: "dead",
    icon: "fa-book-skull",
    buttons: {
      start: true,
      pause: false,
      stop: false,
      unpause: false,
    },
  },
};

function getState({ State }) {
  return Object.values(containerStates).filter(
    ({ key }) => key === State.toLowerCase()
  )[0];
}

function getStateCell(args) {
  const { icon } = getState(args);
  const i = document.createElement("i");
  i.classList.add("fa-solid", icon);
  return i;
}

function getDeleteButton({ Id }) {
  return getDeleteIconButton({
    onclick: (e) => deleteContainer({ Id }).then(alert(`Deleted container`)),
  });
}

function getPauseButton({ Id }) {
  return getPauseIconButton({
    onclick: (e) => pauseContainer({ Id }).then(alert(`Paused container`)),
  });
}

function getStartButton({ Id }) {
  return getStartIconButton({
    onclick: (e) =>
      startContainer({ Id }).then((resp) => alert(`Started container`)),
  });
}

function getStopButton({ Id }) {
  return getStopIconButton({
    onclick: (e) =>
      stopContainer({ Id }).then((resp) => alert(`Stopping container`)),
  });
}

function getUnpauseButton({ Id }) {
  return getUnpauseIconButton({
    onclick: (e) => unpauseContainer({ Id }).then(alert(`Unpaused container`)),
  });
}

function getActionsCell(args) {
  const { buttons } = getState(args);
  const container = document.createElement("span");
  if (buttons.start) {
    container.appendChild(getStartButton(args));
  }
  if (buttons.pause) {
    container.appendChild(getPauseButton(args));
  }
  if (buttons.stop) {
    container.appendChild(getStopButton(args));
  }
  if (buttons.unpause) {
    container.appendChild(getUnpauseButton(args));
  }
  container.appendChild(getDeleteButton(args));
  return container;
}

window["cotainersDynamicTable"] = new Table({
  name: "Available containers",
  columns: [
    {
      name: "Name",
      accessor: ({ Names }) => Names[0].substr(1),
      title: ({ Names }) => Names[0].substr(1),
    },
    {
      name: "Image",
      accessor: "Image",
      title: ({Image}) => Image,
    },
    {
      name: "State",
      accessor: getStateCell,
      style: "text-align: center;",
      title: ({ Status }) => Status,
      width: 53,
    },
    {
      name: "Actions",
      accessor: getActionsCell,
      width: 180,
    },
  ],
  dataFetcher: getContainers,
  reloadSeconds: 5,
  dataCompareFunction: (d1, d2) => {
    return (
      d1.length === d2.length &&
      Object.keys(d1).every((key) => {
        const a = d1[key];
        const b = d2[key];
        return a.Id == b.Id && a.Status == b.Status;
      })
    );
  },
});
