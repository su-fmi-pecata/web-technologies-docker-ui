function deepJsonSearch(data, accessors) {
    const elements = accessors.split(/[\.,\[,\]]/).filter(n => n);
    for (const a of elements) {
        if (data) {
            data = data[a];
        }
    }

    return data;
}

function createHeader(value) {
    const header = document.createElement('h3');
    header.innerText = value;
    return header;
}

function setWidth(elem, width) {
    if (!width) return;
    if (typeof width === 'number') {
        width = `${width}px`;
    }

    elem.style.width = width;
}

function createTableHeaders(columns) {
    const thead = document.createElement('thead');
    const tr = document.createElement('tr');
    for (const { name, width } of columns) {
        const th = document.createElement('th');
        setWidth(th, width);
        th.innerText = name;
        tr.appendChild(th);
    }
    thead.appendChild(tr);
    return thead;
}

function isString(str) {
    return str instanceof String || typeof str === 'string';
}

function generateCell({ data, accessor, style, title }) {
    const td = document.createElement('td');
    if (title) {
        td.title = title(data);
    }

    if (style) {
        td.style = style;
    }

    if (isString(accessor)) {
        td.innerText = deepJsonSearch(data, accessor);
    } else if (accessor instanceof Function) {
        const cellData = accessor(data);
        if (isString(cellData)) {
            td.innerText = cellData;
        } else {
            td.appendChild(accessor(data));
        }
    } else {
        td.innerText = `Unable to process accessor '${accessor}' with given type '${typeof accessor}'`;
    }

    return td;
}

function createTableBody({ data, columns }) {
    const tbody = document.createElement('tbody');
    for (const row of data) {
        const tr = document.createElement('tr');
        for (const col of columns) {
            tr.appendChild(generateCell({ data: row, ...col }));
        }
        tbody.appendChild(tr);
    }
    return tbody;
}

async function createTable({ name, columns, dataFetcher, reloadSeconds, dataCompareFunction }) {
    const table = document.createElement('table');
    const thead = createTableHeaders(columns);
    table.appendChild(thead);

    let data = await dataFetcher();
    let tbody = createTableBody({ data, columns });
    table.appendChild(tbody);

    if (reloadSeconds > 0) {
        let reloadingStarted = false;
        setInterval(async () => {
            if (reloadingStarted) return;
            reloadingStarted = true;

            let d = await dataFetcher();
            if (dataCompareFunction(data, d)) {
                reloadingStarted = false;
                return;
            }
            tbody.remove();

            data = d;
            tbody = createTableBody({ data, columns });
            table.appendChild(tbody);
            reloadingStarted = false;
        }, reloadSeconds * 1000);
    }

    return table;
}

const dynamicTables = document.getElementsByTagName('dynamic-table');
for (const tableElement of dynamicTables) {
    const table = window[tableElement.getAttribute("accessor")];

    tableElement.appendChild(createHeader(table.name));
    tableElement.appendChild(await createTable(table));
}
