import { post, changePage } from './modules/http.mjs';


const form = document.getElementById("register-form");

form.addEventListener("submit", async (event) => {
  event.preventDefault();
  const formData = new FormData(event.target);
  // Build the data object.
  const data = {};
  formData.forEach((value, key) => (data[key] = value));
  const response = await post({ data, url: "/php/user/Register.php", consumeJSON: false });
  //TODO maybe we can present different error messages based on response
  if (response.status === 200) {
    changePage("login");
  } else {
    alert("Registration failed! Try again.")
  }
});