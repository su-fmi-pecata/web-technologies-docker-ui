import { Table } from "./modules/table.mjs";
import { getHosts } from "./modules/docker.mjs";
import { post, changePage } from "./modules/http.mjs";
import { createModal } from "./modules/modal.mjs";
import { getAddIconButton, getIconButton, getInfoIconButton } from "./modules/button.mjs";

const trigger = getAddIconButton({});

function createFormGroup(type, name, label) {
  const formGroup = document.createElement("form-group");

  let inputElement = document.createElement("input");
  inputElement.setAttribute("type", "text");
  inputElement.setAttribute("name", name);
  if (type === "url") {
    inputElement.setAttribute("required", true);
  }

  let labelElement = document.createElement("label");
  labelElement.innerHTML = label;

  formGroup.append(labelElement, inputElement);

  return formGroup;
}

function createHostForm() {
  const form = document.createElement("form");

  const formGroup1 = createFormGroup("url","dockerURL", "URL To Docker Daemon");
  const formGroup2 = createFormGroup("","dockerUsername", "Username To Docker Daemon");
  const formGroup3 = createFormGroup("","dockerPassword", "Password To Docker Daemon");
  // const formGroup4 = document.createElement("form-group");

  form.append(formGroup1, formGroup2, formGroup3);


  let i1 = document.createElement("button");
  i1.setAttribute("class", "btn-accent btn card card-1");
  i1.setAttribute("type", "submit");
  i1.innerText = "Add host config";

  // let span = document.createElement("span");
  // span.innerHTML = "Add host config";

  // i1.append(span);
  form.append(i1);
  // form.append(formGroup4);

  form.addEventListener("submit", async (event) => {
    event.preventDefault();
    const formData = new FormData(event.target);
    // Build the data object.
    const data = {};
    formData.forEach((value, key) => (data[key] = value));
    const response = await post({
      data,
      url: "/php/user/AddHost.php",
      consumeJSON: false,
    });
    console.log(response);
    if (response.status === 200) {
      alert("Host added successfully");
    } else {
      alert("Add host failed! Try again.");
    }
  });

  return form;
}

document
  .getElementById("addHost")
  .appendChild(createModal({ trigger, bodyChild: createHostForm() }));

window["hostsDynamicTable"] = new Table({
  name: "Available hosts",
  columns: [
    {
      name: "URL",
      accessor: "host_url",
    },
    {
      name: "Docker username",
      accessor: "docker_username",
    },
    {
      name: "Actions",
      width: 75,
      style: "text-align: center;",
      accessor: ({ host_url }) => {
        const onclick = async (e) => {
          await post({
            url: "/php/user/SetHost.php",
            data: { host_url },
            consumeJSON: false,
          });
          changePage("home");
        };
        return getIconButton({ title: "Use this host", color: 'success', icon: "fa-angle-right", onclick });
      },
    },
  ],
  dataFetcher: getHosts,
});
