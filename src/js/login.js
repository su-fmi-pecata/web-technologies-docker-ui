import { post, changePage } from "./modules/http.mjs";

const form = document.getElementById("login-form");

form.addEventListener("submit", async (event) => {
  event.preventDefault();
  const formData = new FormData(event.target);
  // Build the data object.
  const data = {};
  formData.forEach((value, key) => (data[key] = value));
  const response = await post({
    data,
    url: "/php/user/Login.php",
    consumeJSON: false,
  });
  if (response.status === 200) {
    changePage("profile");
  } else {
    alert("Wrong credentials! Try again.");
  }
});
