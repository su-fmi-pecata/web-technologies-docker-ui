import {Table} from './modules/table.mjs';
import {deleteVolume, getVolumes} from './modules/docker.mjs';
import {getDeleteIconButton, getInfoIconButton} from "./modules/button.mjs";
import { getRelativeTimeSince } from './modules/time.mjs';
import { createModal } from './modules/modal.mjs';

function getDeleteButton({ Name }) {
  return getDeleteIconButton({
    onclick: (e) =>
      deleteVolume({ Id: Name }).then((resp) => {
        if (resp["message"]) {
          alert(resp["message"]);
        } else {
          alert("Deleted volume");
        }
      }),
  });
}

function getInspectButton({ Name, CreatedAt, Driver, Mountpoint, Scope }) {
    return createModal({
        trigger: getInfoIconButton({ }),
        bodyHtml: `
        <table>
            <col style="width:130px">
            <tr>
                <td>Name</td>
                <td title="${Name}">${Name}</td>
            </tr>
            <tr>
                <td>Created At</td>
                <td title="${CreatedAt}">${CreatedAt}</td>
            </tr>
            <tr>
                <td>Driver</td>
                <td title="${Driver}">${Driver}</td>
            </tr>
            <tr>
                <td>Mountpoint</td>
                <td title="${Mountpoint}">${Mountpoint}</td>
            </tr>
            <tr>
                <td>Scope</td>
                <td title="${Scope}">${Scope}</td>
            </tr>
        </table>
        `,
    });
}

function getActionsCell(args) {
  const container = document.createElement("span");
  container.appendChild(getInspectButton(args));
  container.appendChild(getDeleteButton(args));
  return container;
}

window['volumesDynamicTable'] = new Table({
    name: 'Available volumes',
    columns: [
        {
            name: 'Name',
            accessor: 'Name',
            title: ({Name}) => Name,
        },
        {
            name: 'Created',
            accessor: ({CreatedAt}) => getRelativeTimeSince(new Date(CreatedAt)),
            title: ({CreatedAt}) => new Date(CreatedAt).toISOString(),
            width: 170
        },
        {
            name: "Actions",
            accessor: getActionsCell,
            width: 130,
        },
    ],
    dataFetcher: getVolumes,
    reloadSeconds: 5,
    dataCompareFunction: (d1, d2) => {
        return (
            d1.length === d2.length &&
            Object.keys(d1).every((key) => {
                const a = d1[key];
                const b = d2[key];
                return a.Id == b.Id;
            })
        )
    }
});
