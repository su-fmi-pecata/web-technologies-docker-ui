# WEB Technologies - Docker UI

_Docker UI_ provides you with a web UI using which you can easily interact with the Docker deamon running on your machine.

## Setting up the databse

_Docker UI_ uses a **MYSQL** database. You will need to download and install **MYSQL**. Once you do that, you will need to create a database and configure the [config.json](./src/config.json) file to use your databse. Then, you can execute the migrations by running the [run.sh](./src/migrations/run.sh) like so:

```
./run.sh <db-user> <db-password> <db-name>
```

## Serving project

In order to serve the project make sure the following prerequisites are satisfied:

- You have set up your databse
- You have php installed on your machine
- Your current directory is the root of the project

Once those are satisfied, you can run the following command in order to serve on the project:

```
php -S localhost:6969 -t src/
```
